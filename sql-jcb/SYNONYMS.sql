--################################################################################
--# Nombre del Programa : SYNONYMS.sql                                           #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : P-06-2543-12                        Fecha: 12/Feb/2014 #
--# Descripcion General : sinonimos                                              #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################


CREATE OR REPLACE SYNONYM OPS$REDEXE.PRC_INFRA_JCB_CARG FOR RED.PRC_INFRA_JCB_CARG;
CREATE OR REPLACE SYNONYM OPS$REDEXE.PRC_INFRA_JCB_REV FOR RED.PRC_INFRA_JCB_REV;








 

 
