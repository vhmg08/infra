--################################################################################
--# Nombre del Programa : PRC_INFRA_JCB_CARG.prc                                #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : P-06-2543-12                        Fecha: 19/Dic/2013 #
--# Descripcion General : procedure carga de trx jcb                             #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

  CREATE OR REPLACE PROCEDURE RED.PRC_INFRA_JCB_CARG (P_Fecha_Carga In Date Default Null) is


V_Clasificacion  Char(5);
V_Monto_Min      Number(18,2);
V_Monto_Max      Number(18,2);
V_Mon_Surch_Min  Number(18,2);
v_Mon_Surch_Max  number(18,2);

        Cursor C_Parametros Is
        Select Clasificacion, Monto_Min, Monto_Max, Mon_Surch_Min, Mon_Surch_Max 
        From TBL_CAT_INFRA_INTER
        Where Emisor ='JCB1';


Begin 
      
        Open C_Parametros;
        loop
        
        Fetch C_Parametros Into V_Clasificacion,V_Monto_Min,V_Monto_Max,V_Mon_Surch_Min,v_Mon_Surch_Max;
        EXIT WHEN C_Parametros%NOTFOUND;
            
            Dbms_Output.Put_Line('PARAMETROS   V_Clasificacion  : '||V_Clasificacion || ' V_Monto_Min:  '  || V_Monto_Min || '  V_Monto_Max:  ' || V_Monto_Max || ' V_Mon_Surch_Min:  '|| V_Mon_Surch_Min || ' v_Mon_Surch_Max:  ' || v_Mon_Surch_Max  );
          
        insert into TBL_RED_INFRA  
        Select  
        T.Numero_Cuenta_Acceso,
        T.Numero_Sec_Trans,
        T.Codigo_Atm,
        T.Orden,
        T.Cantidad_Solicitada,
        Substr(Ts.Tkn_Surcharge,20,19),
        T.Cantidad_Balance,
        T.Fecha_Carga, 
        T.Ban_Codigo_Banco, 
        T.Codigo_Banco,
        T.Clave_Operacion,
        T.Codigo_Tipo_Mensaje,
         Case When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance=0 Then  T.Cantidad_Solicitada *-1
                 When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance>0 Then T.Cantidad_Balance - T.Cantidad_Solicitada
            Else T.Cantidad_Solicitada
            End Neto_Dll, 
            V_Clasificacion,   
         Case When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance=0 Then  -1
                 When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance >0 Then 0
            Else 1
            End Tot_Txs
        From Tran_Aux T , TBL_TRAN_JCB Ti, Tbl_Surcharge Ts
        Where t.Fecha_Carga =P_Fecha_Carga
              And TRIM(T.Ban_Codigo_Banco)='JCB1'
              And T.Razon_Respuesta In ('00','01') 
              And T.Tipo_Transaccion<>'21'
              And T.Clave_Operacion In ('10')
              And T.Codigo_Tipo_Cuenta<>'99'
              And Ti.Numero_Cuenta_Acceso(+)=T.Numero_Cuenta_Acceso
              And trim(Ti.Auth_Id(+))=trim(T.Auth_Id)
              And Ti.Numero_Sec_Trans(+) = T.Numero_Sec_Trans
              And Ti.Codigo_Atm(+)=T.Codigo_Atm
              And Ti.orden(+)=T.orden
              And Ts.Fecha_Carga(+)=T.Fecha_Carga
              And Ts.Numero_Cuenta_Acceso(+)=T.Numero_Cuenta_Acceso
              And Ts.Numero_Sec_Trans(+) = T.Numero_Sec_Trans
              And Ts.Codigo_Atm(+)=T.Codigo_Atm
          And Ts.Orden(+)=T.Orden;
      
      End Loop;
      
      close C_Parametros;
      

      end PRC_INFRA_JCB_CARG;
/
 
