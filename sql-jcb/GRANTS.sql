--################################################################################
--# Nombre del Programa : GRANTS.sql                                             #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : P-06-2543-12                        Fecha: 12/Feb/2014 #
--# Descripcion General : Grants                                                 #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

GRANT EXECUTE ON RED.PRC_INFRA_JCB_CARG TO OPS$REDEXE;
GRANT EXECUTE ON RED.PRC_INFRA_JCB_REV TO OPS$REDEXE;


