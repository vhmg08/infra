--################################################################################
--# Nombre del Programa : DAT_CAT_INFRA.sql                                      #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : datos catalogo de coutas                               #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('VISA1','VISA','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),0.65,0.52,0.005,0.2,0.5,null,0,999999.99,0.01,9999999);
Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('MDS2','MDS','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),1.75,null,null,0.2,0.64,null,200,299.99,0,0);
Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('MDS3','MDS','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),2.25,null,null,0.2,0.64,null,300,99999.99,0,0);
Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('MDS4','MDS','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),0.75,null,null,0.2,0.64,null,0,199.99,0.1,99999.99);
Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('MDS5','MDS','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),0.75,null,null,0.2,0.64,null,200,299.99,0.1,99999.99);
Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('MDS6','MDS','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),0.75,null,null,0.2,0.64,null,300,99999.99,0.1,99999.99);
Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('MDS1','MDS','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),1.25,null,null,0.2,0.64,null,0,199.99,0,0);
Insert into RED.TBL_CAT_INFRA_INTER (CLASIFICACION,EMISOR,USER_MOD,FEC_MODIF,CUOTA_FIJA,CUOTA_PORCENT,CUOTA_ADQUIRENTE,CUOTA_SWITCH,CUOTA_OTROS,PARAMETROS_SELEC,MONTO_MIN,MONTO_MAX,MON_SURCH_MIN,MON_SURCH_MAX) values ('VISA2','VISA','RED',to_timestamp('26/11/2013','DD/MM/RRRR HH24:MI:SSXFF'),0.5,0.15,0.005,0.2,0.5,null,0,999999.99,0,0);
COMMIT;