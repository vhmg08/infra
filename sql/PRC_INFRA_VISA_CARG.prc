--################################################################################
--# Nombre del Programa : PRC_INFRA_VISA_CARG.prc                                #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : procedure carga de trx visa                            #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

create or replace
procedure RED.PRC_INFRA_VISA_CARG(P_Fecha_Carga In Date Default Null) is

V_Clasificacion  Char(5);
V_Monto_Min      Number(18,2);
V_Monto_Max      Number(18,2);
V_Mon_Surch_Min  Number(18,2);
v_Mon_Surch_Max  number(18,2);
t_trunca         number;

        Cursor C_Parametros Is
        Select Clasificacion, Monto_Min, Monto_Max, Mon_Surch_Min, Mon_Surch_Max 
        From TBL_CAT_INFRA_INTER
        where emisor ='VISA';


Begin 
        
        sp_trunca_red('truncate table TBL_RED_INFRA',t_trunca);
        
        Open C_Parametros;
        loop
        
        Fetch C_Parametros Into V_Clasificacion,V_Monto_Min,V_Monto_Max,V_Mon_Surch_Min,v_Mon_Surch_Max;
        EXIT WHEN C_Parametros%NOTFOUND;
            
            Dbms_Output.Put_Line('PARAMETROS   V_Clasificacion  : '||V_Clasificacion || ' V_Monto_Min:  '  || V_Monto_Min || '  V_Monto_Max:  ' || V_Monto_Max || ' V_Mon_Surch_Min:  '|| V_Mon_Surch_Min || ' v_Mon_Surch_Max:  ' || v_Mon_Surch_Max  );
          
            
            
        insert into TBL_RED_INFRA
        Select  
        T.Numero_Cuenta_Acceso,
        T.Numero_Sec_Trans,
        T.Codigo_Atm,
        T.Orden,
        Substr(Ti.Tkq5,9,19),
        Substr(Ts.Tkn_Surcharge,20,19),
        t.cantidad_balance,
        T.Fecha_Carga, 
        T.Ban_Codigo_Banco, 
        T.Codigo_Banco,
        T.Clave_Operacion,
        T.Codigo_Tipo_Mensaje,
         Case When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance=0 Then  (To_Number(Trim(Nvl(Substr(Ti.Tkq5,9,19),0)))/100)*-1
                 When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance>0 Then (To_Number(Trim(Nvl(Substr(Ti.Tkq5,9,19),0)))/100 - To_Number(Trim(Nvl(T.Cantidad_Balance,0))))*-1
            Else To_Number(Trim(Nvl(Substr(Ti.Tkq5,9,19),0)))/100
            End Neto_Dll, 
            V_Clasificacion,
        /* Case When  To_Number(trim(Nvl(Substr(Ts.Tkn_Surcharge,20,19),0)))>0 Then  'VISA1'
            Else 'VISA2'
            End Clasificacion,*/
         Case When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance=0 Then  -1
                 When T.Codigo_Tipo_Mensaje='0420' And T.Cantidad_Balance >0 Then 0
            Else 1
            End Tot_Txs 
              From Tran_Aux T , Transacciones_Internacionales Ti, Tbl_Surcharge Ts
        Where t.Fecha_Carga=P_Fecha_Carga
              And T.Ban_Codigo_Banco='VISA'
              And T.Razon_Respuesta In ('00','01') 
              And T.Tipo_Transaccion<>'21'
              And T.Clave_Operacion In ('10')
              And T.Codigo_Tipo_Cuenta<>'99'
              And Substr(Ti.Tkq5,66,3)='840'
               And To_Number(Trim(Nvl(Substr(Ti.Tkq5,9,19),0)))/100 Between V_Monto_Min And V_Monto_Max
              and To_Number(trim(Nvl(Substr(Ts.Tkn_Surcharge,18,13),0))) between V_Mon_Surch_Min and v_Mon_Surch_Max
              And Ti.Numero_Cuenta_Acceso(+)=T.Numero_Cuenta_Acceso
              And trim(Ti.Auth_Id(+))=trim(T.Auth_Id)
              And Ti.Numero_Sec_Trans(+) = T.Numero_Sec_Trans
              And Ti.Codigo_Atm(+)=T.Codigo_Atm
              And Ti.orden(+)=T.orden
              And Ts.Fecha_Carga(+)=T.Fecha_Carga
              And Ts.Numero_Cuenta_Acceso(+)=T.Numero_Cuenta_Acceso
              And Ts.Numero_Sec_Trans(+) = T.Numero_Sec_Trans
              And Ts.Codigo_Atm(+)=T.Codigo_Atm
              And Ts.Orden(+)=T.Orden;
      Commit;
      
      End Loop;
      
      close C_Parametros;
      

      end PRC_INFRA_VISA_CARG;
/
 