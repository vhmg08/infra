--################################################################################
--# Nombre del Programa : TBL_INFRA_TOTALES.sql                                  #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : resultado de cifras infra                              #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

  CREATE TABLE RED.TBL_INFRA_TOTALES (	
    FECHA_CARGA     DATE NOT NULL ENABLE, 
    EMISOR          CHAR(4 BYTE), 
    ADQUIRIENTE     CHAR(4 BYTE), 
    CLAVE_OPERACION CHAR(2 BYTE), 
    CLASIFICACION    CHAR(5 BYTE), 
    MONTO           NUMBER(18,2), 
    NUM_TRANSACC    NUMBER(10,0), 
    MTO_PROMEDIO    NUMBER(18,2)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 40960 NEXT 81920 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "RED_DAT" ;
  
  ALTER TABLE RED.TBL_INFRA_TOTALES ADD CONSTRAINT PK_TBL_INFRA_TOTALES PRIMARY KEY (FECHA_CARGA, EMISOR, ADQUIRIENTE, CLAVE_OPERACION, CLASIFICACION);
  
  COMMENT ON TABLE RED.TBL_INFRA_TOTALES IS 'cifras infraestructura visa, mds';
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.FECHA_CARGA IS 'fecha carga';
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.EMISOR IS 'fiid emisor';  
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.ADQUIRIENTE IS 'fiid adq';
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.CLAVE_OPERACION IS 'retiro, consulta';
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.CLASIFICACION IS 'indicador tipo cuota';
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.MONTO IS 'total cuota';
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.NUM_TRANSACC IS 'cantidad trx couta';
  COMMENT ON COLUMN RED.TBL_INFRA_TOTALES.MTO_PROMEDIO IS 'promedio c';
  
 
