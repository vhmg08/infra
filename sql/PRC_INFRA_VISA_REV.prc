--################################################################################
--# Nombre del Programa : PRC_INFRA_VISA_REV.prc                                 #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : procedure carga de trx visa                            #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

create or replace
Procedure RED.PRC_INFRA_VISA_REV(P_Fecha_Carga In Date Default Null)  is 

vconsecutivo 		         number;
Vcodigo_Atm              Varchar2(10);
Vnumero_Cuenta_Acceso    Varchar2(19);  
Vcodigo_Banco            Char(4);
VBAN_CODIGO_BANCO        CHAR(4);
Vfecha_Carga             Date;
Vnumero_Sec_Trans        Varchar2(12);  
Vclasificacion           Char(5);

cursor reversos is
Select FECHA_CARGA, BAN_CODIGO_BANCO, CODIGO_BANCO, NUMERO_CUENTA_ACCESO, CODIGO_ATM, NUMERO_SEC_TRANS, CLASIFICACION  
From TBL_RED_INFRA
Where (Fecha_Carga, Ban_Codigo_Banco, Codigo_Banco, Numero_Cuenta_Acceso, Codigo_Atm, Numero_Sec_Trans )
In (Select  Fecha_Carga, Ban_Codigo_Banco, Codigo_Banco, numero_cuenta_acceso, codigo_atm, numero_sec_trans 
From TBL_RED_INFRA
Where Fecha_Carga = P_Fecha_Carga 
And Ban_Codigo_Banco ='VISA'
And No_Txs =-1
AND codigo_tipo_mensaje='0420')
and codigo_tipo_mensaje='0210';


begin
  open reversos;
  loop
    Fetch Reversos Into 
    Vfecha_Carga, 
    Vban_Codigo_Banco, 
    Vcodigo_Banco, 
    Vnumero_Cuenta_Acceso, 
    Vcodigo_Atm, 
    Vnumero_Sec_Trans, 
    Vclasificacion;
    
    update TBL_RED_INFRA
    set CLASIFICACION = VCLASIFICACION
			Where Fecha_Carga = Vfecha_Carga
			And Ban_Codigo_Banco= VBAN_CODIGO_BANCO
			and CODIGO_BANCO = VCODIGO_BANCO
			and NUMERO_CUENTA_ACCESO = VNUMERO_CUENTA_ACCESO
			and CODIGO_ATM = VCODIGO_ATM
			and NUMERO_SEC_TRANS = VNUMERO_SEC_TRANS
			And No_Txs =-1
			AND codigo_tipo_mensaje='0420';
    
    exit when reversos%NOTFOUND;

    commit;

  end loop;
close  reversos;


END PRC_INFRA_VISA_REV;
/