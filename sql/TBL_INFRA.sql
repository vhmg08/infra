--################################################################################
--# Nombre del Programa : TBL_INFRA.sql                                          #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : trx para calculo de infra                              #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

  CREATE TABLE RED.TBL_RED_INFRA(
    NUMERO_CUENTA_ACCESO  VARCHAR2(19 BYTE), 
    NUMERO_SEC_TRANS      VARCHAR2(12 BYTE), 
    CODIGO_ATM            VARCHAR2(10 BYTE), 
    ORDEN                 CHAR(4 BYTE), 
    TKQ5                  CHAR(20 BYTE), 
    TKN_SURCHARGE         CHAR(20 BYTE), 
    CANTIDAD_BALANCE      NUMBER(19,2), 
    FECHA_CARGA           DATE NOT NULL ENABLE, 
    BAN_CODIGO_BANCO      CHAR(4 BYTE), 
    CODIGO_BANCO          CHAR(4 BYTE), 
    CLAVE_OPERACION       CHAR(2 BYTE), 
    CODIGO_TIPO_MENSAJE   CHAR(4 BYTE), 
    CANTIDAD_SOLICITADA   NUMBER(18,2), 
    CLASIFICACION         CHAR(5 BYTE), 
    NO_TXS                NUMBER(1,0)
   ) 
   PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 40960 NEXT 81920 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE RED_DAT;
  
  COMMENT ON TABLE RED.TBL_RED_INFRA IS 'temporal para calculo de coutas';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.NUMERO_CUENTA_ACCESO IS 'pan';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.NUMERO_SEC_TRANS IS 'secuencia';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.CODIGO_ATM IS 'nmenomico atm';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.ORDEN IS 'numero orden';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.TKQ5 IS 'token usd';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.TKN_SURCHARGE IS 'surcharge';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.CANTIDAD_BALANCE IS 'importe reverso';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.FECHA_CARGA IS 'fecha carga';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.BAN_CODIGO_BANCO IS 'emisor';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.CODIGO_BANCO IS 'adquirente';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.CLAVE_OPERACION IS 'retiro, consulta';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.CODIGO_TIPO_MENSAJE IS 'retiro, reverso';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.CANTIDAD_SOLICITADA IS 'importe retiro';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.CLASIFICACION IS 'bandera clasificacion';
  COMMENT ON COLUMN RED.TBL_RED_INFRA.NO_TXS IS 'indicador';
  
  
 
