--################################################################################
--# Nombre del Programa : GRANTS.sql                                             #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : Grants                                                 #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

GRANT SELECT, INSERT, UPDATE, DELETE ON RED.TBL_CAT_INFRA_INTER TO OPS$REDEXE;
GRANT SELECT, INSERT, UPDATE, DELETE ON RED.TBL_RED_INFRA TO OPS$REDEXE;
GRANT SELECT, INSERT, UPDATE, DELETE ON RED.TBL_INFRA_TOTALES TO OPS$REDEXE;

GRANT EXECUTE ON RED.PRC_INFRA_VISA_CARG TO OPS$REDEXE;
GRANT EXECUTE ON RED.PRC_INFRA_VISA_REV TO OPS$REDEXE;
GRANT EXECUTE ON RED.PRC_INFRA_MDS_CARG TO OPS$REDEXE;
GRANT EXECUTE ON RED.PRC_INFRA_MDS_REV TO OPS$REDEXE;


