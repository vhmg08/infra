--################################################################################
--# Nombre del Programa : DROP.sql                                               #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : elimina objetos                                        #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

DROP TABLE RED.TBL_CAT_INFRA_INTER;
DROP TABLE RED.TBL_RED_INFRA;
DROP TABLE RED.TBL_INFRA_TOTALES;

DROP PROCEDURE RED.PRC_INFRA_VISA_CARG;
DROP PROCEDURE RED.PRC_INFRA_VISA_REV;
DROP PROCEDURE RED.PRC_INFRA_MDS_CARG;
DROP PROCEDURE RED.PRC_INFRA_MDS_REV;
  