--################################################################################
--# Nombre del Programa : SYNONYMS.sql                                           #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : sinonimos                                              #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################


CREATE OR REPLACE SYNONYM OPS$REDEXE.TBL_CAT_INFRA_INTER FOR RED.TBL_CAT_INFRA_INTER;
CREATE OR REPLACE SYNONYM OPS$REDEXE.TBL_RED_INFRA FOR RED.TBL_RED_INFRA;
CREATE OR REPLACE SYNONYM OPS$REDEXE.TBL_INFRA_TOTALES FOR RED.TBL_INFRA_TOTALES;
CREATE OR REPLACE SYNONYM OPS$REDEXE.PRC_INFRA_VISA_CARG FOR RED.PRC_INFRA_VISA_CARG;
CREATE OR REPLACE SYNONYM OPS$REDEXE.PRC_INFRA_VISA_REV FOR RED.PRC_INFRA_VISA_REV;
CREATE OR REPLACE SYNONYM OPS$REDEXE.PRC_INFRA_MDS_CARG FOR RED.PRC_INFRA_MDS_CARG;
CREATE OR REPLACE SYNONYM OPS$REDEXE.PRC_INFRA_MDS_REV FOR RED.PRC_INFRA_MDS_REV;








 

 
