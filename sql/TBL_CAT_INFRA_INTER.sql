--################################################################################
--# Nombre del Programa : TBL_CAT_INFRA_INTER.sql                                #
--# Compania            : eNova                                                  #
--# Author              : Victor H Montoya G                                     #
--# Proyecto/Procliente : A-03-2867-13                        Fecha: 19/Dic/2013 #
--# Descripcion General : catalogo de coutas                                     #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

CREATE TABLE RED.TBL_CAT_INFRA_INTER
  (                                                                          
     CLASIFICACION     VARCHAR2(5 BYTE) NOT NULL ENABLE,                        
     EMISOR            VARCHAR2(4 BYTE) NOT NULL ENABLE,                        
     CUOTA_FIJA        NUMBER(4,2),                                          
     CUOTA_PORCENT     NUMBER(2,2),                                          
     CUOTA_ADQUIRENTE  NUMBER(2,4),                                          
     CUOTA_SWITCH      NUMBER(2,2),                                          
     CUOTA_OTROS       NUMBER(2,2),                                          
     PARAMETROS_SELEC  VARCHAR2(400 BYTE),                                   
     MONTO_MIN         NUMBER(18,2),                                         
     MONTO_MAX         NUMBER(18,2),                                         
     MON_SURCH_MIN     NUMBER(18,2),                                         
     MON_SURCH_MAX     NUMBER(18,2),
     USER_MOD          VARCHAR2(20 BYTE) NOT NULL ENABLE,                       
     FEC_MODIF         DATE      
  )                                                                          
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE   
  (                                                                          
    INITIAL 40960 NEXT 81920 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0
    FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT                        
  )                                                                          
  TABLESPACE  RED_DAT;
  
  
  ALTER TABLE RED.TBL_CAT_INFRA_INTER ADD CONSTRAINT PK_TBL_CAT_INFRA_INTER PRIMARY KEY (CLASIFICACION);
  
  COMMENT ON TABLE RED.TBL_CAT_INFRA_INTER IS 'catalogo de cuotas';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.CLASIFICACION IS 'ID tipo de couota';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.EMISOR IS 'fiid emisor';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.CUOTA_FIJA IS 'monto cuota fija';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.CUOTA_PORCENT IS 'monto cuota porcentaje';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.CUOTA_ADQUIRENTE IS 'monto cuota adquirente';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.CUOTA_SWITCH IS 'monto cuota switch';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.CUOTA_OTROS IS 'monto cuouta otros';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.PARAMETROS_SELEC IS 'criterios cuota';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.MONTO_MIN IS 'monto minimo';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.MONTO_MAX IS 'monto maximo';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.MON_SURCH_MIN IS 'monto surcharge minimo';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.MON_SURCH_MAX IS 'monto surcharte maximo';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.USER_MOD IS 'usuario modifco registro';
  COMMENT ON COLUMN RED.TBL_CAT_INFRA_INTER.FEC_MODIF IS 'fecha modificacion registro';
  
  
  
  

  
  